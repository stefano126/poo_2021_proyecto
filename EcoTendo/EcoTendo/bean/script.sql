CREATE DATABASE grupo7;

USE grupo7;

CREATE TABLE producto(
 id_producto NUMERIC(9) PRIMARY KEY,
 precio NUMERIC(9,2),
 oferta NUMERIC(9,2),
 nombre VARCHAR(100),
 descripcion VARCHAR(200),
 cantidad NUMERIC(10));
 
 CREATE TABLE imagen_producto(
 id_imagen NUMERIC(9),
 url VARCHAR(200),
 id_producto NUMERIC(9),
 FOREIGN KEY (id_producto) REFERENCES producto(id_producto));
 
 CREATE TABLE usuario(
 codigo NUMERIC(9) PRIMARY KEY,
 nombre VARCHAR(20),
 contraseña VARCHAR(20),
 pais VARCHAR(20),
 direccion VARCHAR(100),
 codigo_postal VARCHAR(100),
 correo_electronico VARCHAR(100),
 num_telef VARCHAR(10));
 
 CREATE TABLE pedido(
 cod_pedido NUMERIC(9) PRIMARY KEY ,
 cod_usuario NUMERIC(9),
 FOREIGN KEY (cod_usuario) REFERENCES usuario(codigo),
 forma_pago VARCHAR(10),
 precio_total NUMERIC(9,2),
 fecha VARCHAR(20),
 sub_total NUMERIC(9,2),
 total_impuestos	NUMERIC(9,2),
 estado VARCHAR(10));
 
 CREATE TABLE categoria(
 codigo_categoria NUMERIC(9) PRIMARY KEY,
 nombre_categoria VARCHAR(100));
 
 CREATE TABLE categoria_producto(
 codigo_categoria NUMERIC(9),
 FOREIGN KEY(codigo_categoria) REFERENCES categoria(codigo_categoria),
 id_producto NUMERIC(9),
 FOREIGN KEY(id_producto) REFERENCES producto(id_producto));
 
 CREATE SEQUENCE secuencia_producto START WITH 1
 
 INSERT INTO producto(id_producto,precio,oferta,nombre,descripcion,cantidad)
 VALUES(NEXTVAL(secuencia_producto),10,5,"Jabon Liquido","Es un jabon liquido :v",3)
 
 CREATE SEQUENCE secuencia_imagen START WITH 1
 
 INSERT INTO imagen_producto(id_imagen,url,id_producto)
 VALUES(NEXTVAL(secuencia_imagen),"https://previews.123rf.com/images/ylivdesign/ylivdesign1702/ylivdesign170205025/71551531-botella-con-icono-de-jab%C3%B3n-l%C3%ADquido-estilo-de-dibujos-animados.jpg",1)
 
 SELECT * FROM imagen_producto
 
 SELECT p.id_producto,p.precio,p.oferta,p.nombre,p.descripcion,p.cantidad,i.id_imagen,i.url
 FROM producto p
 JOIN imagen_producto i ON (i.id_producto=p.id_producto)  
 
 CREATE SEQUENCE secuencia_usuario START WITH 1
 
 INSERT INTO usuario(codigo,nombre,contraseña,pais,direccion,codigo_postal,correo_electronico,num_telef)
 VALUES(NEXTVAL(secuencia_usuario),"Rodrigo","1234","Argentina","Buenos Aires","54321","rodrigom@hotmail.com","524231232")
 
 SELECT nombre,contraseña,pais,direccion,codigo_postal,correo_electronico,num_telef
 FROM usuario
 
 
 



 
 

 
