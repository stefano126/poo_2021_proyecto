package pooproyecto.EcoTendo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcoTendoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcoTendoApplication.class, args);
	}

}
