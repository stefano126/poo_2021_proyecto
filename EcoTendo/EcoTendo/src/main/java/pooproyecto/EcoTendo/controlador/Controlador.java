package pooproyecto.EcoTendo.controlador;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pooproyecto.EcoTendo.dto.RespuestaProducto;
import pooproyecto.EcoTendo.servicio.Servicio;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
	@Autowired
	private Servicio servicio;
    @RequestMapping(value="/obtener-productos",method=RequestMethod.POST,produces="application/json;charset=utf-8")
    public @ResponseBody RespuestaProducto obtenerProductos() throws SQLException {
    	RespuestaProducto respuestaProducto=new RespuestaProducto();
    	respuestaProducto.setProductos(servicio.obtenerProductos());
		return respuestaProducto;
    }
}
