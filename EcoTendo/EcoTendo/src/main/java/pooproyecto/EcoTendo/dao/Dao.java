package pooproyecto.EcoTendo.dao;

import java.sql.SQLException;
import java.util.List;

import pooproyecto.EcoTendo.dto.Producto;

public interface Dao {
    public List<Producto> obtenerProductos() throws SQLException;
}
