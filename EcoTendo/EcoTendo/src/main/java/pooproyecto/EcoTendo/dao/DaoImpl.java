package pooproyecto.EcoTendo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pooproyecto.EcoTendo.dto.Imagen;
import pooproyecto.EcoTendo.dto.Producto;
@Repository
public class DaoImpl implements Dao{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private Connection conexion;
	private void obtenerConexion() throws SQLException {
		conexion=jdbcTemplate.getDataSource().getConnection();
	}
	private void cerrarConexion() throws SQLException {
		conexion.commit();
		conexion.close();
		conexion=null;
	}
	public List<Producto> obtenerProductos() throws SQLException {
		List<Producto> productos=new ArrayList<>();
		String sql=" SELECT p.id_producto id_producto,p.precio precio,p.oferta oferta,p.nombre nombre,p.descripcion descripcion, "
				+ " p.cantidad cantidad,i.id_imagen id_imagen,i.url url"
				+ " FROM producto p"
				+ " JOIN imagen_producto i ON (i.id_producto=p.id_producto)";
		obtenerConexion();
		Statement sentencia=conexion.createStatement();
		ResultSet resultado= sentencia.executeQuery(sql);
		while (resultado.next()) {
			Producto producto=new Producto();
			Imagen imagen=new Imagen();
			imagen.setIdImagen(resultado.getInt("id_imagen"));
			imagen.setUrl(resultado.getString("url"));
			producto.setImagen(imagen);
			producto.setCantidad(resultado.getInt("cantidad"));
			producto.setDescripcion(resultado.getString("descripcion"));
			producto.setNombre(resultado.getString("nombre"));
			producto.setId(resultado.getInt("id_producto"));
			producto.setPrecio(resultado.getDouble("precio"));
			producto.setOferta(resultado.getDouble("oferta"));
			productos.add(producto);
		}
		resultado.close();
		sentencia.close();
		cerrarConexion();
		return productos;
	}

}
