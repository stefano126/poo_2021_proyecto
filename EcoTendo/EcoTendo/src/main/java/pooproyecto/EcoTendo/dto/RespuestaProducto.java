package pooproyecto.EcoTendo.dto;

import java.util.List;

public class RespuestaProducto {
    private List<Producto> productos;

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
}
