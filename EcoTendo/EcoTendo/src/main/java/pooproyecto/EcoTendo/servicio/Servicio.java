package pooproyecto.EcoTendo.servicio;

import java.sql.SQLException;
import java.util.List;

import pooproyecto.EcoTendo.dto.Producto;

public interface Servicio {
      public List<Producto> obtenerProductos() throws SQLException;
}
