package pooproyecto.EcoTendo.servicio;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pooproyecto.EcoTendo.dao.Dao;
import pooproyecto.EcoTendo.dto.Producto;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
	Dao dao;
	public List<Producto> obtenerProductos(){
		List<Producto> lista=new ArrayList<>();
		try {
			lista=dao.obtenerProductos();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}
	
       
}
