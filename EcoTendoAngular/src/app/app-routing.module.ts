import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CatalogoComponent} from "./catalogo/catalogo.component";
import {InicioComponent } from './inicio/inicio.component';
import { InformacionComponent } from './informacion/informacion.component';
const routes: Routes = [{path:"catalogo",component: CatalogoComponent},{path:"inicio",component: InicioComponent},{path:"informacion",component:InformacionComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
