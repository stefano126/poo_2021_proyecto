import { Component, OnInit } from '@angular/core';
import {RespuestaProducto, Producto } from '../interfaces';
import { ApiService } from '../ApiService';
@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss']
})
export class CatalogoComponent implements OnInit {
   productos:Producto[]=[];
   producto?:Producto;
  constructor(private api:ApiService) {}

  ngOnInit(): void {
    this.api.obtenerProductos().subscribe(data=>{
      this.productos=data.listaProducto;
    });
  }

}
