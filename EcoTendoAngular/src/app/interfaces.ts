export interface Imagen{
	idImagen: number;
    url: string;
    idProducto: number;
}
export interface Producto{
	id: number;
	precio: number;
    oferta: number;
    nombre: string;
    descripcion: string;
    cantidad: number;
    imagen: Imagen;
}
export interface RespuestaProducto{
	listaProducto: Producto[];
}
